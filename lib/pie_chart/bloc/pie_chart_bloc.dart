import 'package:flutter_bloc/flutter_bloc.dart';

import '../api/pie_chart_api.dart';
import 'pie_chart_event.dart';
import 'pie_chart_state.dart';

class PieChartBloc extends Bloc<PieChartEvent, PieChartState> {
  PieChartBloc() : super(PieChartInitial()) {
    final apiRepo = PieChartApiProvider();

    on<FetchPieChartData>(
      (event, emit) async {
        emit(PieChartLoading());
        try {
          final res = await apiRepo.fetchPieChart();

          // if (res.errorMsg == null || res.pieChartData!.isNotEmpty) {
          //   res.pieChartData!.sort((a, b) => a.value.compareTo(b.value));

          //   for (int i = 0;
          //       i < pieChartColors.length && i < res.pieChartData!.length;
          //       i++) {
          //     res.pieChartData![i].color = pieChartColors[i];
          //   }
          // }

          emit(PieChartLoaded(res));
          if (res.errorMsg != null) {
            emit(PieChartError(res.errorMsg));
          }
        } catch (e) {
          emit(PieChartError("Failed to fetch data"));
        }
      },
    );
  }
}
