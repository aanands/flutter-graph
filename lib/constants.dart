import 'package:flutter/material.dart';

String companyId = "45948319441";

String baseURL = "https://d2zav8hj4uqmfg.cloudfront.net/v1/$companyId";

List<Color> pieChartColors = const [
  Color(0x0016163f),
  Color(0x002f5f98),
  Color(0x002b8bba),
  Color(0x0040b9d5),
  Color(0x006ce5e8),
  Color(0x00d190a8),
  Color(0x00feb9bd),
  Color(0x00d690aa),
  Color(0x00d690aa),
  Color(0x00704e85)
];

Map<String, List<dynamic>> cateogriesMap = {
  "Food": const [
    Icon(Icons.restaurant),
    Color.fromRGBO(255, 219, 219, 1),
  ],
  "Petrol": const [
    Icon(Icons.local_gas_station),
    Color.fromRGBO(195, 209, 217, 1),
  ],
  "Hotel": const [
    Icon(Icons.hotel),
    Color.fromRGBO(176, 247, 177, 1),
  ],
  "Taxi": const [
    Icon(Icons.local_taxi),
    Color.fromRGBO(255, 251, 167, 1),
  ],
  "Tickets": const [
    Icon(Icons.wallet_rounded),
    Color.fromRGBO(250, 205, 220, 1),
  ],
  "Stationary": const [
    Icon(Icons.shopping_bag),
    Color.fromRGBO(255, 222, 184, 1),
  ],
  "Electricity": const [
    Icon(Icons.power),
    Color.fromRGBO(212, 253, 255, 1),
  ],
  "Repair/Maintainence": const [
    Icon(Icons.home_repair_service),
    Color.fromRGBO(255, 210, 195, 1),
  ],
  "Telecom & Internet": const [
    Icon(Icons.public),
    Color.fromRGBO(0, 133, 255, 1),
  ],
  "Computer & Software": const [
    Icon(Icons.computer),
    Color.fromRGBO(255, 215, 215, 1),
  ],
  "Rent": const [
    Icon(Icons.car_rental),
    Color.fromRGBO(246, 246, 246, 1),
  ],
  "Insurance": const [
    Icon(Icons.health_and_safety_sharp),
    Color.fromRGBO(220, 224, 238, 1),
  ],
  "General Expenses": const [
    Icon(Icons.payment),
    Color.fromRGBO(252, 199, 151, 1),
  ],
  "Transport costs": const [
    Icon(Icons.directions_bus),
    Color.fromRGBO(255, 210, 195, 1),
  ],
  "Labor": const [
    Icon(Icons.work),
    Color.fromRGBO(242, 162, 165, 1),
  ],
  "Spares": const [
    Icon(Icons.savings),
    Color.fromRGBO(130, 245, 170, 1),
  ],
  "Contractor": const [
    Icon(Icons.document_scanner),
    Color.fromRGBO(245, 205, 169, 1),
  ],
};

Map monthNumber = {
  "April": 1,
  "May": 2,
  "June": 3,
  "July": 4,
  "August": 5,
  "September": 6,
  "October": 7,
  "November": 8,
  "December": 9,
  "January": 10,
  "February": 11,
  "March": 12,
};
