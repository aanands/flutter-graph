import 'package:http/http.dart' as http;
import 'dart:convert';

import '../../constants.dart';
import '../model/bar_chart_model.dart';

class BarApiProvider {
  Future<BarChartModel> fetchBarGraph(String cat) async {
    dynamic res;
    try {
      String encodedCat = Uri.encodeComponent(cat);

      res = await http
          .get(
            Uri.parse(
                "$baseURL/get_month_breakdown_by_category?category=$encodedCat"),
          )
          .timeout(
            const Duration(seconds: 5),
          );
    } catch (e) {
      return BarChartModel.withError("Failed to fetch data");
    }
    if (res.statusCode == 200) {
      return BarChartModel.fromJson(json.decode(res.body));
    }
    return BarChartModel.withError("Failed to fetch data");
  }
}
