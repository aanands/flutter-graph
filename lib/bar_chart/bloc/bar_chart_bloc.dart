import 'package:flutter_bloc/flutter_bloc.dart';

import '../api/bar_graph_api.dart';
import 'bar_chart_event.dart';
import 'bar_chart_state.dart';

class BarChartBloc extends Bloc<FetchBarChartData, BarChartState> {
  BarChartBloc() : super(BarChartInitial()) {
    final apiRepo = BarApiProvider();

    on<FetchBarChartData>(
      (event, emit) async {
        emit(BarChartLoading());
        try {
          final res = await apiRepo.fetchBarGraph(event.cat);

          res.barChartData!.sort(
            (a, b) => a.monthVal.compareTo(b.monthVal),
          );

          emit(BarChartLoaded(res));
          if (res.errorMsg != null) {
            emit(BarChartError(res.errorMsg));
          }
        } catch (e) {
          emit(BarChartError("Failed to fetch data"));
        }
      },
    );
  }
}
