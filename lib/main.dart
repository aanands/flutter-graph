import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tezbooks/pie_chart/pie_chart_screen.dart';
import 'package:tezbooks/bar_chart/bloc/bar_chart_bloc.dart';
import 'package:tezbooks/pie_chart/bloc/pie_chart_bloc.dart';

void main() async {
  runApp(const TezBooksApp());
}

class TezBooksApp extends StatefulWidget {
  const TezBooksApp({super.key});

  @override
  State<TezBooksApp> createState() => _TezBooksAppState();
}

class _TezBooksAppState extends State<TezBooksApp> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        appBarTheme: const AppBarTheme(
          elevation: 0,
          backgroundColor: Colors.transparent,
          iconTheme: IconThemeData(
            color: Colors.black,
          ),
          titleTextStyle: TextStyle(
            color: Colors.black,
            fontSize: 24,
            fontWeight: FontWeight.w500,
          ),
        ),
        iconTheme: const IconThemeData(
          color: Colors.black,
        ),
      ),
      home: MultiBlocProvider(
        providers: [
          BlocProvider(
            create: (_) => PieChartBloc(),
          ),
          BlocProvider(
            create: (_) => BarChartBloc(),
          ),
        ],
        child: const PieChartScreen(),
      ),
    );
  }
}
