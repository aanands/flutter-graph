import 'package:flutter/material.dart';
import 'dart:math' as math;

import '../../constants.dart';

class PieChartNewData {
  String name;
  double value;
  dynamic icon;
  Color color;
  Color iconColor;

  PieChartNewData({
    required this.name,
    required this.value,
    required this.icon,
    required this.iconColor,
    required this.color,
  });
}

class PieChartModel {
  List<PieChartNewData>? pieChartData;
  dynamic sum;
  String? errorMsg;

  PieChartModel.fromJson(Map<String, dynamic> json) {
    pieChartData = [];
    sum = json["data"]["sum"];

    for (int i = 0; i < json["data"]["data"].length; i++) {
      List catList = cateogriesMap[json["data"]["data"][i][0].toString()] ??
          [
            Text(
              json["data"]["data"][i][0][0],
              textAlign: TextAlign.center,
              style: const TextStyle(
                fontSize: 22,
              ),
            ),
            Color((math.Random().nextDouble() * 0xFFFFFF).toInt())
                .withOpacity(0.75),
          ];
      PieChartNewData temp = PieChartNewData(
        name: json["data"]["data"][i][0],
        value: json["data"]["data"][i][1].toDouble(),
        icon: catList[0],
        iconColor: catList[1],
        color: pieChartColors[i % 10],
      );
      pieChartData!.add(temp);
    }
  }

  PieChartModel.withError(String error) : errorMsg = error;
}
