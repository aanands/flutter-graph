import 'package:http/http.dart' as http;
import 'dart:convert';

import '../../constants.dart';
import '../model/pie_chart_model.dart';

class PieChartApiProvider {
  Future<PieChartModel> fetchPieChart() async {
    dynamic res;
    try {
      res = await http
          .get(Uri.parse("$baseURL/get_yearly_totals?platform=flutter"))
          .timeout(
            const Duration(seconds: 5),
          );
    } catch (e) {
      return PieChartModel.withError("Failed to fetch data");
    }
    if (res.statusCode == 200) {
      return PieChartModel.fromJson(json.decode(res.body));
    }
    return PieChartModel.withError("Failed to fetch data");
  }
}
