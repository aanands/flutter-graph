abstract class PieChartEvent {}

class FetchPieChartData extends PieChartEvent {}

class TouchPieChart extends PieChartEvent {
  int index;
  TouchPieChart(this.index);
}
