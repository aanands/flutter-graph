import '../../constants.dart';

class BarChartNewData {
  int monthVal;
  String monthName;
  double value;

  BarChartNewData({
    required this.monthVal,
    required this.value,
    required this.monthName,
  });
}

class BarChartModel {
  List<BarChartNewData>? barChartData;
  String? errorMsg;

  BarChartModel.fromJson(Map<String, dynamic> json) {
    barChartData = [];

    monthNumber.forEach((key, value) {
      barChartData!
          .add(BarChartNewData(monthVal: value, value: 0, monthName: key));
    });

    for (int i = 0; i < json["data"]["data"].length; i++) {
      barChartData!
          .elementAt(monthNumber[json["data"]["data"][i][0]] - 1)
          .value = json["data"]["data"][i][1].toDouble();
    }
  }

  BarChartModel.withError(String error) : errorMsg = error;
}
