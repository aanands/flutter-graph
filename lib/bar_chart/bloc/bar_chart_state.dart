import '../model/bar_chart_model.dart';

abstract class BarChartState {}

class BarChartInitial extends BarChartState {}

class BarChartLoading extends BarChartState {}

class BarChartLoaded extends BarChartState {
  final BarChartModel barChartModel;
  BarChartLoaded(this.barChartModel);
}

class BarChartError extends BarChartState {
  final String? error;
  BarChartError(this.error);
}
