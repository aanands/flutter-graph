import '../model/pie_chart_model.dart';

abstract class PieChartState {}

class PieChartInitial extends PieChartState {}

class PieChartLoading extends PieChartState {}

class PieChartLoaded extends PieChartState {
  final PieChartModel pieChartModel;
  PieChartLoaded(this.pieChartModel);
}

class PieChartError extends PieChartState {
  final String? error;
  PieChartError(this.error);
}
