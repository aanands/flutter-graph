import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../bar_chart/bar_chart_screen.dart';
import 'bloc/pie_chart_bloc.dart';
import 'bloc/pie_chart_event.dart';
import 'bloc/pie_chart_state.dart';
import 'widget/indicator.dart';

class PieChartScreen extends StatefulWidget {
  const PieChartScreen({super.key});

  @override
  State<PieChartScreen> createState() => _PieChartScreenState();
}

class _PieChartScreenState extends State<PieChartScreen> {
  final PieChartBloc pieChartBloc = PieChartBloc();
  @override
  void initState() {
    pieChartBloc.add(FetchPieChartData());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double ratioHeight = 737 / MediaQuery.of(context).size.height;
    double ratioWidth = 392 / MediaQuery.of(context).size.width;

    return RefreshIndicator(
      onRefresh: () async {
        pieChartBloc.add(FetchPieChartData());
      },
      child: Scaffold(
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Colors.transparent,
        ),
        body: SafeArea(
          child: BlocBuilder<PieChartBloc, PieChartState>(
            bloc: pieChartBloc,
            builder: (context, state) {
              if (state is PieChartLoaded) {
                if (state.pieChartModel.pieChartData!.isEmpty) {
                  return Stack(
                    children: <Widget>[
                      Container(
                        decoration: const BoxDecoration(
                          image: DecorationImage(
                            image:
                                ExactAssetImage('assets/loading_pie_chart.png'),
                            fit: BoxFit.cover,
                          ),
                        ),
                        child: BackdropFilter(
                          filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
                          child: Container(
                            decoration: BoxDecoration(
                                color: Colors.white.withOpacity(0.0)),
                          ),
                        ),
                      ),
                      Center(
                        child: Text(
                          "Please upload expenses\nto view insights ",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: (24 * ratioWidth),
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      )
                    ],
                  );
                }
                return SingleChildScrollView(
                  child: Column(
                    children: [
                      Row(
                        children: <Widget>[
                          const SizedBox(
                            height: 18,
                          ),
                          Expanded(
                            child: AspectRatio(
                              aspectRatio: 1,
                              child: PieChart(
                                PieChartData(
                                  pieTouchData: PieTouchData(
                                    touchCallback:
                                        (FlTouchEvent event, pieTouchResponse) {
                                      if (!event.isInterestedForInteractions ||
                                          pieTouchResponse == null ||
                                          pieTouchResponse.touchedSection ==
                                              null) {
                                        return;
                                      }
                                    },
                                  ),
                                  borderData: FlBorderData(
                                    show: false,
                                  ),
                                  sectionsSpace: 0,
                                  centerSpaceRadius: 60 * ratioWidth,
                                  sections: List<PieChartSectionData>.generate(
                                    state.pieChartModel.pieChartData!.length,
                                    (index) {
                                      int percentage = int.parse((state
                                                  .pieChartModel
                                                  .pieChartData![index]
                                                  .value /
                                              state.pieChartModel.sum!.toInt() *
                                              100)
                                          .toStringAsFixed(0));
                                      String percentageText =
                                          percentage > 10 ? "$percentage%" : "";
                                      return PieChartSectionData(
                                          title: percentageText,
                                          radius: 40 * ratioWidth,
                                          value: state.pieChartModel
                                              .pieChartData![index].value,
                                          color: state.pieChartModel
                                              .pieChartData![index].color,
                                          showTitle: true,
                                          titleStyle: TextStyle(
                                            fontSize: 12 * ratioWidth,
                                            fontWeight: FontWeight.bold,
                                          ));
                                    },
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              SizedBox(
                                width: 100,
                                child: ListView.separated(
                                  itemCount:
                                      state.pieChartModel.pieChartData!.length,
                                  shrinkWrap: true,
                                  separatorBuilder: (context, index) {
                                    return const SizedBox(
                                      height: 10,
                                    );
                                  },
                                  itemBuilder: (context, index) {
                                    return Indicator(
                                      color: state.pieChartModel
                                          .pieChartData![index].color,
                                      text: state.pieChartModel
                                          .pieChartData![index].name,
                                      ratioWidth: ratioWidth,
                                    );
                                  },
                                ),
                              ),
                            ],
                          ),
                          const SizedBox(
                            width: 28,
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(
                              left: 20,
                              bottom: 10,
                            ),
                            child: Align(
                              alignment: Alignment.centerLeft,
                              child: Text(
                                "Categories ",
                                style: TextStyle(
                                  fontSize: 20 * ratioWidth,
                                  color: Colors.black,
                                ),
                              ),
                            ),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          SingleChildScrollView(
                            physics: const AlwaysScrollableScrollPhysics(),
                            child: ListView.separated(
                              itemCount:
                                  state.pieChartModel.pieChartData!.length,
                              shrinkWrap: true,
                              scrollDirection: Axis.vertical,
                              physics: const NeverScrollableScrollPhysics(),
                              separatorBuilder: (context, index) {
                                return const SizedBox(
                                  height: 15,
                                );
                              },
                              itemBuilder: (context, index) {
                                int val = state
                                    .pieChartModel.pieChartData![index].value
                                    .toInt();
                                return GestureDetector(
                                  onTap: () {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) => BarChartScreen(
                                          category: state.pieChartModel
                                              .pieChartData![index].name,
                                        ),
                                      ),
                                    );
                                  },
                                  child: Container(
                                    margin: const EdgeInsets.only(
                                      left: 15,
                                      right: 15,
                                    ),
                                    padding: const EdgeInsets.all(12),
                                    decoration: BoxDecoration(
                                      border: Border.all(
                                        color: Colors.black54,
                                        width: 0.5,
                                      ),
                                    ),
                                    child: Row(
                                      children: [
                                        Expanded(
                                          flex: 0,
                                          child: Container(
                                            width: 35,
                                            height: 35,
                                            alignment: Alignment.center,
                                            decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(10),
                                              shape: BoxShape.rectangle,
                                              color: state
                                                  .pieChartModel
                                                  .pieChartData![index]
                                                  .iconColor,
                                            ),
                                            child: Center(
                                              child: state.pieChartModel
                                                  .pieChartData![index].icon,
                                            ),
                                          ),
                                        ),
                                        const SizedBox(
                                          width: 10,
                                        ),
                                        Expanded(
                                          flex: 8,
                                          child: Container(
                                            margin:
                                                const EdgeInsets.only(left: 5),
                                            child: Align(
                                              alignment: Alignment.centerLeft,
                                              child: Text(
                                                state.pieChartModel
                                                    .pieChartData![index].name,
                                                textAlign: TextAlign.left,
                                                overflow: TextOverflow.ellipsis,
                                                style: TextStyle(
                                                  fontWeight: FontWeight.w600,
                                                  fontSize: 16 * ratioWidth,
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                        Expanded(
                                          flex:
                                              "\u{20B9} ${state.pieChartModel.pieChartData![index].value}"
                                                      .length ~/
                                                  2,
                                          child: Align(
                                            alignment: Alignment.centerRight,
                                            child: Text(
                                              "\u{20B9}$val",
                                              textAlign: TextAlign.left,
                                              style: TextStyle(
                                                fontWeight: FontWeight.w600,
                                                fontSize: 16 * ratioWidth,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                );
                              },
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                );
              }
              if (state is PieChartError) {
                return Center(
                  child: Text(
                    state.error!,
                    style: TextStyle(
                      fontSize: 24 * ratioWidth,
                    ),
                  ),
                );
              }
              return const Center(
                child: CircularProgressIndicator(),
              );
            },
          ),
        ),
      ),
    );
  }
}
