import 'dart:ui';

import 'package:fl_chart/fl_chart.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'bloc/bar_chart_bloc.dart';
import 'bloc/bar_chart_event.dart';
import 'bloc/bar_chart_state.dart';
import 'model/bar_chart_model.dart';

class BarChartScreen extends StatefulWidget {
  BarChartScreen({
    super.key,
    required this.category,
  });

  String category;

  @override
  State<BarChartScreen> createState() => _BarChartScreenState();
}

class _BarChartScreenState extends State<BarChartScreen> {
  final BarChartBloc barChartBloc = BarChartBloc();

  @override
  void initState() {
    super.initState();

    barChartBloc.add(FetchBarChartData(widget.category));
  }

  String formatNumber(int number) {
    return NumberFormat.compactCurrency(
      decimalDigits: 2,
      symbol: '',
    ).format(number);
  }

  int touchedIndex = -1;

  @override
  Widget build(BuildContext context) {
    double ratioHeight = 737 / MediaQuery.of(context).size.height;
    double ratioWidth = 392 / MediaQuery.of(context).size.width;

    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        // TODO Adithya Appbar is present, Change it to transparent when using
        backgroundColor: Colors.blue,
      ),
      body: SafeArea(
        child: BlocBuilder<BarChartBloc, BarChartState>(
          bloc: barChartBloc,
          builder: (context, state) {
            if (state is BarChartLoaded) {
              if (state.barChartModel.barChartData!.isEmpty) {
                return Stack(
                  children: <Widget>[
                    Container(
                      decoration: const BoxDecoration(
                        image: DecorationImage(
                          image:
                              ExactAssetImage('assets/loading_bar_chart.png'),
                          fit: BoxFit.cover,
                        ),
                      ),
                      child: BackdropFilter(
                        filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
                        child: Container(
                          decoration: BoxDecoration(
                              color: Colors.white.withOpacity(0.0)),
                        ),
                      ),
                    ),
                    Center(
                      child: Text(
                        "Please upload expenses\nto view insights ",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: (24 * ratioWidth),
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    )
                  ],
                );
              }
              BarChartNewData maxEle = state.barChartModel.barChartData!
                  .reduce((a, b) => a.value > b.value ? a : b);

              return SingleChildScrollView(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: AspectRatio(
                        aspectRatio: 1.4,
                        child: BarChart(
                          BarChartData(
                            alignment: BarChartAlignment.spaceEvenly,
                            borderData: FlBorderData(
                              show: true,
                              border: const Border.symmetric(
                                horizontal: BorderSide(
                                  color: Color(0xFFececec),
                                ),
                              ),
                            ),
                            titlesData: FlTitlesData(
                              show: true,
                              leftTitles: AxisTitles(
                                drawBehindEverything: true,
                                sideTitles: SideTitles(
                                  showTitles: true,
                                  getTitlesWidget: (value, meta) {
                                    return Text(
                                      formatNumber(value.toInt()),
                                      style: TextStyle(
                                        color: const Color(0xFF606060),
                                        // TODO: Adithya Font size smaller user not readable
                                        fontSize: 8 * ratioWidth,
                                      ),
                                      textAlign: TextAlign.left,
                                    );
                                  },
                                ),
                              ),
                              rightTitles: AxisTitles(),
                              topTitles: AxisTitles(),
                              bottomTitles: AxisTitles(
                                sideTitles: SideTitles(
                                  showTitles: true,
                                  reservedSize: 30,
                                  getTitlesWidget: (value, meta) {
                                    return Text(
                                      state
                                          .barChartModel
                                          .barChartData![value.toInt()]
                                          .monthName
                                          .toString()
                                          .substring(0, 1),
                                      style: const TextStyle(
                                        color: Color(0xFF606060),
                                      ),
                                      textAlign: TextAlign.left,
                                    );
                                  },
                                ),
                              ),
                            ),

                            gridData: FlGridData(
                              show: true,
                              drawVerticalLine: false,
                              getDrawingHorizontalLine: (value) => FlLine(
                                color: const Color(0xFFececec),
                                strokeWidth: 0,
                              ),
                            ),
                            barGroups: List<BarChartGroupData>.generate(
                              state.barChartModel.barChartData!.length,
                              (index) => BarChartGroupData(
                                x: index,
                                barsSpace: 0.1,
                                barRods: [
                                  BarChartRodData(
                                    toY: state.barChartModel
                                        .barChartData![index].value,
                                    color: const Color.fromRGBO(22, 22, 63, 1),
                                    width: 20 * ratioWidth,
                                    borderRadius:
                                        const BorderRadius.all(Radius.zero),
                                  ),
                                ],
                              ),
                            ),
                            // maxY: maxEle.value + 5000,
                            barTouchData: BarTouchData(
                              enabled: true,
                              handleBuiltInTouches: false,
                              touchCallback:
                                  (FlTouchEvent event, barTouchResponse) {
                                setState(() {
                                  if (!event.isInterestedForInteractions ||
                                      barTouchResponse == null ||
                                      barTouchResponse.spot == null) {
                                    touchedIndex = -1;
                                    return;
                                  }
                                  print(touchedIndex);
                                  touchedIndex = barTouchResponse
                                      .spot!.touchedBarGroupIndex;
                                });
                              },
                              touchTooltipData: BarTouchTooltipData(
                                tooltipBgColor: Colors.transparent,
                                tooltipMargin: 0,
                                getTooltipItem: (
                                  BarChartGroupData group,
                                  int groupIndex,
                                  BarChartRodData rod,
                                  int rodIndex,
                                ) {
                                  return BarTooltipItem(
                                    rod.toY.toString(),
                                    TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: rod.color,
                                      fontSize: 18 * ratioWidth,
                                      shadows: const [
                                        Shadow(
                                          color: Colors.black26,
                                          blurRadius: 12,
                                        )
                                      ],
                                    ),
                                  );
                                },
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(
                            left: 20,
                            bottom: 10,
                          ),
                          child: Align(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              "${widget.category} ",
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                fontSize: 20 * ratioWidth,
                                color: Colors.black,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                          ),
                        ),
                        SingleChildScrollView(
                          physics: const AlwaysScrollableScrollPhysics(),
                          child: ListView.separated(
                            itemCount: state.barChartModel.barChartData!.length,
                            shrinkWrap: true,
                            scrollDirection: Axis.vertical,
                            physics: const NeverScrollableScrollPhysics(),
                            separatorBuilder: (context, index) {
                              if (state.barChartModel.barChartData![index]
                                      .value !=
                                  0) {
                                return const SizedBox(
                                  height: 15,
                                );
                              }
                              return const SizedBox.shrink();
                            },
                            itemBuilder: (context, index) {
                              if (state.barChartModel.barChartData![index]
                                      .value !=
                                  0) {
                                int val = state
                                    .barChartModel.barChartData![index].value
                                    .toInt();
                                return Container(
                                  margin: const EdgeInsets.only(
                                    left: 15,
                                    right: 15,
                                  ),
                                  padding: const EdgeInsets.all(12),
                                  decoration: BoxDecoration(
                                    border: Border.all(
                                      color: Colors.black54,
                                      width: 0.5,
                                    ),
                                  ),
                                  child: Row(
                                    children: [
                                      Expanded(
                                        flex: 0,
                                        child: Container(
                                          width: 35,
                                          height: 35,
                                          decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                            shape: BoxShape.rectangle,
                                            color: const Color.fromRGBO(
                                                245, 205, 169, 1),
                                          ),
                                          child: Center(
                                            child: Text(
                                              state
                                                  .barChartModel
                                                  .barChartData![index]
                                                  .monthName[0],
                                              style: TextStyle(
                                                fontSize: 22 * ratioWidth,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                      const SizedBox(
                                        width: 10,
                                      ),
                                      Expanded(
                                        flex: 0,
                                        child: Container(
                                          margin:
                                              const EdgeInsets.only(left: 5),
                                          child: Align(
                                            alignment: Alignment.centerLeft,
                                            child: Text(
                                              state
                                                  .barChartModel
                                                  .barChartData![index]
                                                  .monthName
                                                  .toString(),
                                              textAlign: TextAlign.left,
                                              style: TextStyle(
                                                fontWeight: FontWeight.w600,
                                                fontSize: 16 * ratioWidth,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        flex:
                                            "\u{20B9} ${state.barChartModel.barChartData![index].value}"
                                                    .length ~/
                                                2,
                                        child: Container(
                                          margin:
                                              const EdgeInsets.only(left: 5),
                                          child: Align(
                                            alignment: Alignment.centerRight,
                                            child: Text(
                                              "\u{20B9} $val",
                                              textAlign: TextAlign.left,
                                              style: TextStyle(
                                                fontWeight: FontWeight.w600,
                                                fontSize: 16 * ratioWidth,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                );
                              }
                              return const SizedBox.shrink();
                            },
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              );
            }
            if (state is BarChartError) {
              return Center(
                child: Text(state.error!),
              );
            }
            return const Center(
              child: CircularProgressIndicator(),
            );
          },
        ),
      ),
    );
  }
}
